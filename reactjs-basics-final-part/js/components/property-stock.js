import React from 'react';
import PropertyStockList from './property-stock-list';

var propertyStockData = [
  {id: 1, address: "12 Atlantic Avenue", price: "450000", rentalIncome: "400"},
  {id: 2, address: "23 Baltic Avenue", price: "550000", rentalIncome: "490"},
  {id: 3, address: "43 Connecticut Avenue", price: "350000", rentalIncome: "300"},
  {id: 4, address: "47 Illinois Avenue", price: "764000", rentalIncome: "640"},
  {id: 5, address: "743 Indiana Avenue", price: "578000", rentalIncome: "510"},
  {id: 6, address: "88 Indiana Avenue", price: "639000", rentalIncome: "535"},
  {id: 7, address: "41 Marvin Gardens ", price: "556000", rentalIncome: "490"},
  {id: 8, address: "99 Mediterranean Avenue", price: "375000", rentalIncome: "430"},
  {id: 9, address: "8 New York Avenue", price: "1350000", rentalIncome: "970"},
  {id: 10, address: "55 North Carolina Avenue", price: "465000", rentalIncome: "500"}
];

var thisComponent;

export default class PropertyStock extends React.Component {

  constructor(props) {
        super(props);

        thisComponent = this;

        this.state = {
          data: [],
          totalPrice: 0,
          totalRentalIncome: 0
        };
  }

  componentDidMount () {
    console.log('componentDidMount');

    PubSub.subscribe( 'MyPropertiesSold', function(msg, data){
        console.log('MyPropertiesSold:' + data.address)
        console.log(thisComponent);
        var houses = thisComponent.state.data;
        houses.push(data);
        thisComponent.setState({data: houses}, function(){
            thisComponent.computeTotals();
        });
    });

    thisComponent.setState({data: propertyStockData}, function(){
      thisComponent.computeTotals();
    });
  }

  removeItemId (index) {
    var houses = thisComponent.state.data;
    var itemRemoved = thisComponent.state.data[index];
    console.log('itemRemoved:' + itemRemoved.address);
    PubSub.publish( 'StocksBought', itemRemoved );
    houses.splice(index,1);
    thisComponent.setState({data: houses}, function(){
        thisComponent.computeTotals();
    });
  }

  computeTotals () {
    console.log('computeTotals: ' + thisComponent.state.data.length );
    var sumPrice = 0;
    var sumRentalIncome = 0;
    thisComponent.state.data.forEach(
      function addNumber(value) {
        sumPrice += parseInt(value.price);
        sumRentalIncome += parseInt(value.rentalIncome);
      }
    );
    thisComponent.setState({
      totalPrice: sumPrice,
      totalRentalIncome: sumRentalIncome
    });
  }

  render () {
    return (
      <div>
          <h2>Property Stock</h2>
          <table className="table table-hover table-bordered">
              <thead>
                  <tr className="info">
                      <th>Address</th>
                      <th>Price</th>
                      <th>Rent (per week)</th>
                      <th>Action</th>
                  </tr>
              </thead>
            <PropertyStockList data={thisComponent.state.data}
              removeItem={thisComponent.removeItemId}
              totalPrice={thisComponent.state.totalPrice}
              totalRentalIncome={thisComponent.state.totalRentalIncome}/>
          </table>
      </div>
    );
  }
}
