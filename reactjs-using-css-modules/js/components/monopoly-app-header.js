import React from 'react'
import styleable from 'react-styleable'
import css from './monopoly-app-header.css'

export default styleable(css) (class MonopolyApp extends React.Component {
  render() {
    return (
      <div className={css.root}>Monopoly Real Estate</div>
    );
  }
})
