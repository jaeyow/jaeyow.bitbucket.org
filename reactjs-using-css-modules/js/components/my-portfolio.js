
import React from 'react'
import MyPortfolioList from './my-portfolio-list'
import styleable from 'react-styleable'
import css from './my-portfolio.css'

var myPropertyData = [];
var thisComponent;

export default styleable(css) (class MyPortfolio extends React.Component {

  constructor (props) {
        super(props);

        thisComponent = this;

        thisComponent.state = {
          data: [],
          totalPrice: 0,
          totalRentalIncome: 0
        };
  }

  componentDidMount () {

    PubSub.subscribe( 'StocksBought', function(msg, data){
      console.log('StocksBought:' + data.address);
      var houses = thisComponent.state.data;
      houses.push(data);
      thisComponent.setState({data: houses}, function(){
          thisComponent.computeTotals();
      });
    });
    thisComponent.setState({data: myPropertyData}, function(){
      thisComponent.computeTotals();
    });
  }

  removeItemId (index) {
    var houses = thisComponent.state.data;
    var itemRemoved = thisComponent.state.data[index];
    console.log('itemRemoved:' + itemRemoved.address);
    PubSub.publish( 'MyPropertiesSold', itemRemoved);
    houses.splice(index,1);
    thisComponent.setState({data: houses}, function(){
      thisComponent.computeTotals();
    });
  }

  computeTotals (){
    console.log('computeTotals: ' + thisComponent.state.data.length );
    var sumPrice = 0;
    var sumRentalIncome = 0;
    thisComponent.state.data.forEach(
      function addNumber(value) {
        sumPrice += parseInt(value.price);
        sumRentalIncome += parseInt(value.rentalIncome);
      }
    );
    thisComponent.setState({
      totalPrice: sumPrice,
      totalRentalIncome: sumRentalIncome
    });
  }

  render () {
    return (
      <div className={css.tableBackground}>
          <div className={css.root}>My Portfolio</div>
          <table className="table table-hover table-bordered">
              <thead>
                  <tr className={css.headerBackground}>
                      <th className={css.center}>Address</th>
                      <th className={css.center}>Price</th>
                      <th className={css.center}>Rent (per week)</th>
                      <th className={css.center}>Action</th>
                  </tr>
              </thead>
                <MyPortfolioList data={thisComponent.state.data}
                  removeItem={thisComponent.removeItemId}
                  totalPrice={thisComponent.state.totalPrice}
                  totalRentalIncome={thisComponent.state.totalRentalIncome}/>
          </table>
      </div>
    );
  }
})
