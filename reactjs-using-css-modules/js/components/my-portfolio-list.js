import React from 'react'

export default class MyPortfolioList extends React.Component {

  constructor (props) {
        super(props);
  }

  handleClick (index, event) {
    this.props.removeItem(index);
  }

  render () {
  var createItem = function(dataItem, index) {
     return (
       <tr key={dataItem.id}>
         <td>{dataItem.address}</td>
         <td>$ {dataItem.price}</td>
         <td>$ {dataItem.rentalIncome}</td>
         <td>
             <button className="btn btn-success btn-sm" onClick={this.handleClick.bind(this, index)}>Sell</button>
         </td>
      </tr>
    );
   };

   return (
      <tbody>
        {this.props.data.map(createItem, this)}
        <tr key="folioTotals">
          <td>Totals</td>
          <td>$ {this.props.totalPrice}</td>
          <td>$ {this.props.totalRentalIncome}</td>
          <td></td>
        </tr>
      </tbody>
     );
   }
 }
