import React from 'react'
import ReactDOM from 'react-dom'
import MonopolyApp from './monopoly-app-header'
import PropertyStock from './property-stock'
import MyPortfolio from './my-portfolio'

import css from './app.css'

ReactDOM.render(
  <div>
      <MonopolyApp />
      <PropertyStock />
      <MyPortfolio />
  </div>,
  document.getElementById('monopoly-app')
);
