var webpack = require('webpack');

module.exports = {
    devtool: 'inline-source-map',
    entry: [
       'webpack-dev-server/client?http://127.0.0.1:8080/',
       'webpack/hot/only-dev-server',
        './js/components/app.js'
    ],
    output: {
        path: __dirname,
        filename: 'bundle.js'
    },
    resolve: {
        modulesDirectories: ['node_modules', 'src'],
        extensions: ['', '.js', '.css']
    },
    module: {
        loaders: [
        {
            test: /\.js?$/,
            exclude: /node_modules/,
            loaders: ['react-hot', 'babel?presets[]=react,presets[]=es2015']
        },
        {
          test: /\.css/,
          loaders:['style', 'css?modules&localIdentName=[local]---[hash:base64:5]', 'cssnext']
        }
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin()
    ]
};
